from flask import Flask, render_template
from flask_socketio import SocketIO
import smbus2
from helper import *

ARDUINO_ADDRESS = 0x04
I2Cbus = smbus2.SMBus(1)

app = Flask(__name__)
socketio = SocketIO(app)

@app.route("/")
def index():
    return render_template('index.html')

@socketio.on('command')
def handle_json_button(message):
    I2Cbus.write_i2c_block_data(ARDUINO_ADDRESS, 0, convertToMotors(message['ax0'], message['ax1']))
    #print(message)

@socketio.on('client_connected')
def handle_client_connect_event(json):
    print('received json: {0}'.format(str(json)))



if __name__ == '__main__':
    socketio.run(app, port=5000, host='0.0.0.0')
