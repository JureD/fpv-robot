Repozitorij za izdelavo robota, ki ga lahko upravnljamo preko interneta.

Raspberry Pi na robotu servira spletno stran, do katere dostopamo na oddaljenem računalniku,
robota upravljamo z igralnim kontrolerjem (testirano z X-Box), ki ga priključimo na oddaljen računalnik, nato računalnik preko websocket-a komunicira s strežnikom (Raspberry Pi) in mu pošilja ukaze za premikanje po prostoru.