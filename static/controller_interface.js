const debug = true;

var socket = io.connect('http://' + document.domain + ':' + location.port);
socket.on('connect', function() {
    // we emit a connected message to let knwo the client that we are connected.
    socket.emit('client_connected', {'connected': true});
});

var gamepadInfo = document.getElementById("gamepad-info");


let a = 0;
let b = 0;



// Function for connection to gamepad
window.addEventListener("gamepadconnected", function(e) {
  var gp = navigator.getGamepads()[0];
  document.getElementById("controller_prompt").innerHTML = `Controller connected! Type: ${gp.id}`;
  if (debug) {
    console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
    gp.index, gp.id,
    gp.buttons.length, gp.axes.length);
  }
  console.log(gp);
  buttonLoopPoller();
});


// Function for disconnection from gamepad
window.addEventListener("gamepaddisconnected", function(e) {
  document.getElementById("controller_prompt").innerHTML = 'Please, connect the controller!';
  if (debug) {
    console.log("Gamepad disconnected from index %d: %s",
    e.gamepad.index, e.gamepad.id);
  }
});

function buttonLoopPoller(){
  setInterval(buttonLoop, 50);
}

function buttonPressed(btn) {
  if (typeof(btn) == 'object') {
    return btn.pressed;
  }
}


function buttonLoop() {
  var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []);
  if (!gamepads) {
    return;
  }

  var gp = gamepads[0];

  let data = {};

  for (let i = 0; i < gp.buttons.length; i++) {
    data[`btn${i}`] = gp.buttons[i].pressed;
    if (buttonPressed(gp.buttons[i])) {
      if (debug){
        console.log(`Button ${i} pressed`);
      }
    }
  }
  for (let i = 0; i < gp.axes.length; i++) {
    data[`ax${i}`] = gp.axes[i];
    if (debug) {
      console.log(`Axes ${gp.axes[i]} position`);
    }
  }
  socket.emit('command', data);
}

