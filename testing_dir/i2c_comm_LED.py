import smbus2
import time

ARDUINO_ADDRESS = 0x04

import smbus2

ARDUINO_ADDRESS = 0x04

I2Cbus = smbus2.SMBus(1)

LED1brightness = 0
LED1ascending = True
LED2brightness = 255
LED2ascending = False

while True:
    I2Cbus.write_i2c_block_data(ARDUINO_ADDRESS, 0, [LED1brightness, LED2brightness])
    time.sleep(0.001)

    if LED1ascending:
        LED1brightness += 1
    else:
        LED1brightness -= 1

    if LED2ascending:
        LED2brightness += 1
    else:
        LED2brightness -= 1

    if (LED1brightness == 0 or LED1brightness == 255):
        LED1ascending = not(LED1ascending)
    
    if (LED2brightness == 0 or LED2brightness == 255):
        LED2ascending = not(LED2ascending)