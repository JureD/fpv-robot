import RPi.GPIO as gpio
import time

ledPin = 14

gpio.setmode(gpio.BCM)
gpio.setup(ledPin, gpio.OUT)

while True:
    gpio.output(ledPin, 1)
    time.sleep(1)
    gpio.output(ledPin, 0)
    time.sleep(1)
