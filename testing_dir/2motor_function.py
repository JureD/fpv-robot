

def convertToMotors(x, y):
    sign = lambda x: (1, -1)[x < 0]

    motor1 = (sign(x) * x**2) - (sign(y) * y**2)
    motor2 = -(sign(y) * y**2) - (sign(x) * x**2)
    return (motor1, motor2)