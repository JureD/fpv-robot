import smbus2
import time

ARDUINO_ADDRESS = 0x04

I2Cbus = smbus2.SMBus(1)

stepper1speed = 128
stepper1ascending = True

while True:
    I2Cbus.write_i2c_block_data(ARDUINO_ADDRESS, 0, [stepper1speed, 0])
    time.sleep(0.05)

    if stepper1ascending:
        stepper1speed += 1
    else:
        stepper1speed -= 1

    if (stepper1speed == 0 or stepper1speed == 255):
        stepper1ascending = not(stepper1ascending)
