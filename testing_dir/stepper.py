import RPi.GPIO as gpio
import time

stepperPins = (5, 6, 13, 19)

gpio.setmode(gpio.BCM)
gpio.setup(stepperPins, gpio.OUT)

phases=((0, 0, 0, 1),
	(0, 0, 1, 1),
	(0, 0, 1, 0),
	(0, 1, 1, 0),
	(0, 1, 0, 0),
	(1, 1, 0, 0),
	(1, 0, 0, 0),
	(1, 0, 0, 1))

offState = (0, 0, 0, 0)

steps = 1000
currentStep = 0

while currentStep < steps:
    for phase in phases:
        print(phase)
        gpio.output(stepperPins, phase)
    currentStep += 1
    time.sleep(0.01)

gpio.cleanup()
