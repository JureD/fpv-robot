def convertToMotors(x, y):
    sign = lambda x: (1, -1)[x < 0]

    motor1 = (sign(x) * x**2) - (sign(y) * y**2)
    motor2 = -(sign(y) * y**2) - (sign(x) * x**2)

    motor1 = int((motor1 + 1) * 127.5)
    motor2 = int((motor2 + 1) * 127.5)
    return (motor1, motor2)